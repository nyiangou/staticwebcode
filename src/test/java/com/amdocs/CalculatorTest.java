package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;
//comments
public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        final int result= new Calculator().add();
        assertEquals("Add", 9, result);
        
    }
    @Test
    public void testSub() throws Exception {

        final int result= new Calculator().sub();
        assertEquals("Sub", 3, result);

    }
}
