package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;
//comments
public class IncrementTest {
    @Test
    public void testdecreasecounter() throws Exception {

        final int result= new Increment().decreasecounter(5);
        assertEquals("Increment", 2, result);
        
    }

@Test
public void testdecreasecounterzero() throws Exception {

        final int result= new Increment().decreasecounter(0);
        assertEquals("Increment", 0, result);
        
    }
   
}
